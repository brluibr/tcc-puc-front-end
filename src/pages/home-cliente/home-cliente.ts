import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { HomePage } from '../home/home';
import { AlterarClientePage } from '../alterar-cliente/alterar-cliente';
import { Cliente } from '../../Models/cliente';
import { ClienteServiceProvider } from '../../providers/cliente-service/cliente-service';
import { HttpErrorResponse } from '../../../node_modules/@angular/common/http';

@IonicPage()
@Component({
  selector: 'page-home-cliente',
  templateUrl: 'home-cliente.html',
})
export class HomeClientePage {

  public cliente: Cliente;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    private _clienteProvider: ClienteServiceProvider, private _alertCtrl: AlertController) {
    this.cliente = this.navParams.get('clienteLogado')
  }

  sair() {

    this._alertCtrl.create({
      subTitle: 'Deseja realmente sair ?',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancelar',
        },
        {
          text: 'Confirmar',
          handler: data => {
            this.navCtrl.setRoot(HomePage, {
            });
          },
        },
      ]
    }).present();
  };

  excluir() {

    this._alertCtrl.create({
      title: 'Aviso',
      subTitle: 'Deseja realmente excluir Cliente?',
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancelar',
        },
        {
          text: 'Confirmar',
          handler: () => {
            this._clienteProvider.excluirCliente(this.cliente)
              .subscribe(
                (cliente) => {
                  this._alertCtrl.create({
                    title: 'Exclusão com sucesso',
                    subTitle: 'Cliente excluído com sucesso',
                    buttons: [{
                      text: 'OK',
                      handler: () => {
                        this.navCtrl.setRoot(HomePage);
                      }
                    }]
                  }).present();
                },
                (err: HttpErrorResponse) => {
                  this._alertCtrl.create({
                    title: 'Falha na conexão',
                    subTitle: 'Não foi possivel efetuar a operação, tente novamente mais tarde',
                    buttons: [{
                      text: 'OK',
                    }]
                  }).present();
                }
              );
          }
        }
      ]
    }).present();
  };

  alterar() {
      this.navCtrl.push(AlterarClientePage, {
        cliente: this.cliente
      });
  };
}
