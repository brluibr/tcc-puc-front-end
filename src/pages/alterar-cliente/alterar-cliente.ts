import { HttpErrorResponse } from '@angular/common/http';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { Cliente } from '../../Models/cliente';
import { ClienteServiceProvider } from '../../providers/cliente-service/cliente-service';

@IonicPage()
@Component({
  selector: 'page-alterar-cliente',
  templateUrl: 'alterar-cliente.html',
})
export class AlterarClientePage {

  public cliente: Cliente;
  public endereco: string;
  public estado: string;
  public municipio: string;
  public telefone: string;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    private _clienteProvider: ClienteServiceProvider, private _alertCtrl: AlertController) {
      this.cliente = this.navParams.get('cliente'),
      this.endereco = this.cliente.endereco,
      this.estado = this.cliente.estado,
      this.municipio = this.cliente.municipio,
      this.telefone = this.cliente.telefone
  }

  alterar() {

    if (!this.endereco || !this.estado || !this.municipio || !this.telefone) {
      this._alertCtrl.create({
        title: 'Preenchimento Obrigatório',
        subTitle: 'Favor preencher todos os campos',
        buttons: [{
          text: 'OK',
        }]
      }).present();
      return;
    }

    let regExp = /^((\\+971-?)|0)?[0-9]{11}$/;

    if (!regExp.test(this.telefone)) {
      this._alertCtrl.create({
        title: 'Telefone invalido',
        subTitle: 'Favor digitar um telefone valido - formato DDD + numero do celular ou fixo sem traços ou espaços',
        buttons: [{
          text: 'OK',
        }]
      }).present();
      return;
    }

    this.cliente.endereco = this.endereco;
    this.cliente.estado = this.estado;
    this.cliente.municipio = this.municipio;
    this.cliente.telefone = this.telefone;

    let mensagem = '';

    this._clienteProvider.cadastarCliente(this.cliente)
      .finally(
        () => {
          this._alertCtrl.create({
            title: 'Aviso',
            subTitle: mensagem,
            buttons: [{
              text: 'OK',
            }]
          }).present();
        }
      )
      .subscribe(
        (cliente) => {
          mensagem = 'Dados alterados com sucesso';
        },
        (err: HttpErrorResponse) => {
          mensagem = 'Não foi possivel efetuar a operação, tente novamente mais tarde';
        }
      );
  }
}
