import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController } from 'ionic-angular';
import { Cliente } from '../../Models/cliente';
import { ClienteServiceProvider } from '../../providers/cliente-service/cliente-service';
import { HttpErrorResponse } from '@angular/common/http';
import { HomePage } from '../home/home';

@IonicPage()
@Component({
  selector: 'page-cadastro-cliente',
  templateUrl: 'cadastro-cliente.html',
})
export class CadastroClientePage {

  public cpf: string = '';
  public nome: string = '';
  public endereco: string = '';
  public estado: string = '';
  public municipio: string = '';
  public telefone: string = '';
  public email: string = '';
  public senha: string = '';
  public senha2: string = '';

  DECIMAL_SEPARATOR = ".";
  GROUP_SEPARATOR = ",";
  pureResult: any;
  maskedId: any;
  val: any;
  v: any;

  constructor(public navCtrl: NavController, public navParams: NavParams,
    private _clienteProvider: ClienteServiceProvider, private _alertCtrl: AlertController,
    private _loadingCtrl: LoadingController) {
  }

  format(valString) {
    if (!valString) {
      return '';
    }
    let val = valString.toString();
    const parts = this.unFormat(val).split(this.DECIMAL_SEPARATOR);
    this.pureResult = parts;
    return this.cpf_mask(parts[0]);
  };

  unFormat(val) {
    if (!val) {
      return '';
    }
    val = val.replace(/\D/g, '');

    if (this.GROUP_SEPARATOR === ',') {
      return val.replace(/,/g, '');
    } else {
      return val.replace(/\./g, '');
    }
  };

  cpf_mask(v) {
    v = v.replace(/\D/g, ''); //Remove tudo o que não é dígito
    v = v.replace(/(\d{3})(\d)/, '$1.$2'); //Coloca um ponto entre o terceiro e o quarto dígitos
    v = v.replace(/(\d{3})(\d)/, '$1.$2'); //Coloca um ponto entre o terceiro e o quarto dígitos
    //de novo (para o segundo bloco de números)
    v = v.replace(/(\d{3})(\d{1,2})$/, '$1-$2'); //Coloca um hífen entre o terceiro e o quarto dígitos
    return v;
  }


  cadastrar() {

    if (!this.cpf || !this.nome || !this.endereco || !this.estado || !this.municipio || !this.telefone ||
      !this.email || !this.senha || !this.senha2) {
      this._alertCtrl.create({
        title: 'Preenchimento Obrigatório',
        subTitle: 'Favor preencher todos os campos',
        buttons: [{
          text: 'OK',
        }]
      }).present();
      return;
    }

    if (this.cpf.length < 14 || this.cpf.length > 14) {
      this._alertCtrl.create({
        title: 'CPF invalido',
        subTitle: 'favor informar somente os 11 digitos numericos do CPF',
        buttons: [{
          text: 'OK',
        }]
      }).present();
      return;
    }

    let regExp = /^((\\+971-?)|0)?[0-9]{11}$/;

    if (!regExp.test(this.telefone)) {
      this._alertCtrl.create({
        title: 'Telefone invalido',
        subTitle: 'Favor digitar um telefone valido - formato DDD + numero do celular ou fixo sem traços ou espaços',
        buttons: [{
          text: 'OK',
        }]
      }).present();
      return;
    }

    let re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (!re.test(this.email)) {
      this._alertCtrl.create({
        title: 'email invalido',
        subTitle: 'Favor digitar um email valido',
        buttons: [{
          text: 'OK',
        }]
      }).present();
      return;
    }

    if (this.senha != this.senha2) {
      this._alertCtrl.create({
        title: 'Senha Invalida',
        subTitle: 'Senha de confirmação diferente da senha informada',
        buttons: [{
          text: 'OK',
        }]
      }).present();
      return;
    }

    let cliente: Cliente = {
      cpf: this.cpf,
      nome: this.nome,
      endereco: this.endereco,
      estado: this.estado,
      municipio: this.municipio,
      telefone: this.telefone,
      email: this.email,
      senha: this.senha
    }

    let load = this._loadingCtrl.create({
      content: "processando..."
    });

    load.present();

    this._clienteProvider.cadastarCliente(cliente)
      .finally(
        () => {
          load.dismiss();
        }
      )
      .subscribe(
        (cliente) => {
          this._alertCtrl.create({
            title: 'Cadastro com sucesso',
            subTitle: 'Cliente cadastrado com sucesso',
            buttons: [{
              text: 'OK',
              handler: () => {
                this.navCtrl.setRoot(HomePage);
              }
            }]
          }).present();
        },
        (err: HttpErrorResponse) => {
          console.log(err);
          if (err.status == 400) {
            this._alertCtrl.create({
              title: 'Login não realizado',
              subTitle: 'Usuario e/ou senha invalido',
              buttons: [{
                text: 'OK',
              }]
            }).present();
          }
          else {
            this._alertCtrl.create({
              title: 'Falha na conexão',
              subTitle: 'Não foi possivel efetuar login, tente novamente mais tarde',
              buttons: [{
                text: 'OK',
              }]
            }).present();
          }
        }
      );
  }
}
