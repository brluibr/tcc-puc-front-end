import { HttpErrorResponse } from '@angular/common/http';
import { Component } from '@angular/core';
import { AlertController, LoadingController, NavController } from 'ionic-angular';
import { ClienteServiceProvider } from '../../providers/cliente-service/cliente-service';
import { CadastroClientePage } from '../cadastro-cliente/cadastro-cliente';
import { HomeClientePage } from '../home-cliente/home-cliente';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  public email: string = '';
  public senha: string = '';

  constructor(public navCtrl: NavController, private _clienteProvider: ClienteServiceProvider, 
    private _loadingCtrl: LoadingController, private _alertCtrl: AlertController) {
    
  }
  
  cadastrar(){
    this.navCtrl.push(CadastroClientePage);  
  };

  logar(){
    console.log(this.email);
    console.log(this.senha);

    let login = {
      email: this.email,
      senha: this.senha
    }

    let load = this._loadingCtrl.create({
      content: "efetuando login..."
    });
    
    load.present();

    this._clienteProvider.loginCliente(login)
        .finally(
          () =>{
          load.dismiss();
          }
        )
        .subscribe(
          (cliente) => {
            this.navCtrl.setRoot(HomeClientePage,{
              clienteLogado: cliente
            });
          },
          (err: HttpErrorResponse) =>{
            if (err.status == 400){
              this._alertCtrl.create({
                title: 'Login não realizado',
                subTitle: 'Usuario e/ou senha invalido',
                buttons: [{
                  text: 'OK',
                }]
              }).present();
            }
            else{
              this._alertCtrl.create({
                title: 'Falha na conexão',
                subTitle: 'Não foi possivel efetuar login, tente novamente mais tarde',
                buttons: [{
                  text: 'OK',
                }]
              }).present();
            }
            
          } 
        );
  }
}
