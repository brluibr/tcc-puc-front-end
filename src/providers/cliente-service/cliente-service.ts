import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'

@Injectable()
export class ClienteServiceProvider {
  
  private urlCadastro = "https://tccpuc.herokuapp.com/cliente"
  private urlExcluir = "https://tccpuc.herokuapp.com/excluir"
  private urlConsulta = "https://tccpuc.herokuapp.com/login/"

  constructor(private _http: HttpClient) {
    console.log('Hello ClienteServiceProvider Provider');
  }
  
  cadastarCliente(cliente) {
    return this._http.post(this.urlCadastro, cliente)
  }

  loginCliente(login) {
    return this._http.post(this.urlConsulta, login)
  }

  excluirCliente(cliente) {
    return this._http.post(this.urlExcluir, cliente);
  }


}
