export interface Cliente {
    cpf: string;
    nome: string;
    endereco: string;
    estado: string;
    municipio: string;
    telefone: string;
    email: string;
    senha: string;
}
