import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { HttpClientModule } from '@angular/common/http';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { CadastroClientePage } from '../pages/cadastro-cliente/cadastro-cliente';
import { ClienteServiceProvider } from '../providers/cliente-service/cliente-service';
import { AlterarClientePage } from '../pages/alterar-cliente/alterar-cliente';
import { HomeClientePage } from '../pages/home-cliente/home-cliente';
import 'rxjs/add/operator/finally'

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    CadastroClientePage,
    HomeClientePage,
    AlterarClientePage
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    IonicModule.forRoot(MyApp)
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    CadastroClientePage,
    HomeClientePage,
    AlterarClientePage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    ClienteServiceProvider
  ]
})
export class AppModule {}
